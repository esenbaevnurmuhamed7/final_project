terraform {
  backend "s3" {
    bucket = "final-project-bootcamp"
    key    = "terraform/dev/"
    region = "us-east-1"
  }
}
